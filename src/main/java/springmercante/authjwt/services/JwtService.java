package springmercante.authjwt.services;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@Service
public class JwtService {

  // Clave secreta para firmar el token (en formato Base64)
  private static final String SECRET_KEY="586E3272357538782F413F4428472B4B6250655368566B597033733676397924";

  // Generar y retornar un token JWT a partir del objeto UserDetails
  public String getToken(UserDetails user) {
    return getToken(new HashMap<>(), user);
  }

  // Generar y retornar un token JWT con posibilidad de incluir detalles adicionales
  private String getToken(Map<String, Object> extraClaims, UserDetails user) {
    return Jwts
      .builder()
      .setClaims(extraClaims)
      .setSubject(user.getUsername())
      .setIssuedAt(new Date(System.currentTimeMillis()))
      .setExpiration(new Date(System.currentTimeMillis()+1000*60*24))
      .signWith(getkey(), SignatureAlgorithm.HS256)
      .compact();
  }

  // Obtener la clave para firmar el token a partir de la clave secreta codificada en Base64
  private Key getkey() {
    byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
    return Keys.hmacShaKeyFor(keyBytes);
  }

  // Obtener el nombre de usuario (subject) desde un token JWT
  public String getUsernameFromToken(String token) {
    return getClaim(token, Claims::getSubject);
  }

  // Verificar si el token JWT es válido para un UserDetails especificado
  public boolean isTokenValid(String token, UserDetails userDetails) {
    final String username = getUsernameFromToken(token);
    return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
  }

  // Obtener todos los detalles (claims) desde un token JWT
  private Claims getAllClaims(String token) {
    return Jwts
      .parserBuilder()
      .setSigningKey(getkey())
      .build()
      .parseClaimsJws(token)
      .getBody();
  }

  // Obtener una reclamación específica desde un token JWT
  public <T> T getClaim(String token, Function<Claims, T> claimsResolver) 
  {
    final Claims claims = getAllClaims(token);
    return claimsResolver.apply(claims);
  }

  // Obtener la fecha de expiración de un token JWT
  private Date getExpiration(String token)
  {
    return getClaim(token, Claims::getExpiration);
  }

  // Resolver si el token ha expirado
  private boolean isTokenExpired(String token) 
  {
    return getExpiration(token).before(new Date());
  }
}
